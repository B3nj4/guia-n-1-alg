#ifndef AMINOACIDO_H
#define AMINOACIDO_H
using namespace std;

//Librerias necesarias
#include <string>
#include <list>
#include "Atomo.h"

class Aminoacido {
    private:
        //Variables
        string nombre;
        int numero;
        list<Atomo> atomos;

    public:
        //Constructor
        Aminoacido();
        Aminoacido(string nombre, int numero);

        //Metodos
        string get_nombre();
        int get_numero();
        list<Atomo> get_atomos();
        void set_nombre(string nombreNuevo);
        void set_numero(int numeroNuevo);
        void add_atomo(Atomo atomoNuevo);
   
};
#endif
