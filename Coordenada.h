#ifndef COORDENADA_H
#define COORDENADA_H
using namespace std;

class Coordenada {
    private:
        //Variables
        float x;
        float y;
        float z;

    public:
        //Constructor
        Coordenada();
        Coordenada(float x, float y, float z);

        //Metodos
        float get_x();
        float get_y();
        float get_z();
        void set_x(float xNuevo);
        void set_y(float yNuevo);
        void set_z(float zNuevo);
   
};
#endif
