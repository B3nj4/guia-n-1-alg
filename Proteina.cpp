#include <iostream>
using namespace std;

//Librerias necesarias
#include <list>

//Importe de archivos necesarios
#include "Proteina.h"

//Variables
Proteina::Proteina() {
    string nombre = "\0";
    string id = "\0";
    list<Cadena> cadenas;
}
    
//Constructor
Proteina::Proteina(string nombre, string id){
    this->nombre = nombre;
    this->id = id;
} 

//Metodos
string Proteina::get_id(){
    return this->id;
}
string Proteina::get_nombre(){
    return this->nombre;
}  
list<Cadena> Proteina::get_cadenas(){
    return this->cadenas;
}    
    
void Proteina::set_id(string idNuevo) {
    id = idNuevo;
}
void Proteina::set_nombre(string nombreNuevo) {
    nombre = nombreNuevo;
}
void Proteina::add_cadena(Cadena cadenaNuevo) {
    cadenas.push_back(cadenaNuevo);
}

