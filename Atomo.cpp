#include <iostream>
using namespace std;

//Librerias necesarias
#include <string>
#include <list>

//Importe de archivos necesarios
#include "Atomo.h"
#include "Coordenada.h"

//Variables
Atomo::Atomo() {
    string nombre = "\0";
    int numero = 0;
    Coordenada coordenada;
}

//Constructor
Atomo::Atomo(string nombre, int numero){
    this->nombre = nombre;
    this->numero = numero;
}

//Metodos
string Atomo::get_nombre(){
    return this->nombre;
}
int Atomo::get_numero(){
    return this->numero;
}
Coordenada Atomo::get_coordenada(){
    return this->coordenada;
}

void Atomo::set_nombre(string nombreNuevo) {
    nombre = nombreNuevo;
}
void Atomo::set_numero(int numeroNuevo) {
    numero = numeroNuevo;
}
void Atomo::set_coordenada(Coordenada coordenadaNuevo){
    coordenada = coordenadaNuevo;
}
   