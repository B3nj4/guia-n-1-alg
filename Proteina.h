#ifndef PROTEINA_H
#define PROTEINA_H
using namespace std;

//Librerias necesarias
#include <string>
#include <list>
#include "Cadena.h"


class Proteina {
    private:
        //Variables
        string nombre = "\0";
        string id = "\0";
        list<Cadena> cadenas;

    public:
        //Constructor
        Proteina();
        Proteina(string nombre, string id);

        //Metodos
        string get_id();
        string get_nombre();
        list<Cadena> get_cadenas();
        void set_id(string idNuevo);
        void set_nombre(string nombreNuevo);
        void add_cadena(Cadena cadenaNuevo);

};
#endif
