#include <iostream>
using namespace std;

//Librerias necesarias
#include <list>

//Importe de archivos necesarios
#include "Aminoacido.h"
#include "Atomo.h"

//Variables
Aminoacido::Aminoacido() {
    string nombre = "\0";
    int numero = 0;
    list<Atomo> atomos;
}

//Constructor
Aminoacido::Aminoacido(string nombre, int numero){
        this->nombre = nombre;
        this->numero = numero;
}

//Metodos
string Aminoacido::get_nombre(){
    return this->nombre;
}
int Aminoacido::get_numero(){
    return this->numero;
}    
list<Atomo> Aminoacido::get_atomos(){
    return this->atomos;
}   
    
void Aminoacido::set_nombre(string nombreNuevo) {
    nombre = nombreNuevo;
}
void Aminoacido::set_numero(int numeroNuevo) {
    numero = numeroNuevo;
}
void Aminoacido::add_atomo(Atomo atomoNuevo){
    atomos.push_back(atomoNuevo);    
}
