#include <iostream>
using namespace std;

//Librerias necesarias
#include <list>
#include <cstdlib>
#include <string.h>

//Inclusion de las otras clases
#include "Proteina.h"
#include "Cadena.h"
#include "Aminoacido.h"
#include "Atomo.h"
#include "Coordenada.h"


//Función para imprimir la informacion de alguna proteina en particular 
void imprime_proteina(list <Proteina> proteinas, string seleccion_proteina) {
    for (Proteina prot : proteinas) { 
        if (seleccion_proteina == prot.get_nombre()){
            cout << "\n- ID de la proteina: " << prot.get_id() << "\n- Nombre de la proteina: " << prot.get_nombre();
            
            for (Cadena cadena: prot.get_cadenas()){
                cout << "\n- Cadena: " << cadena.get_letra();
                for (Aminoacido aminoacido: cadena.get_aminoacidos()) {
                    cout << "\n\t Aminoacido -> " << aminoacido.get_nombre() << "-" << aminoacido.get_numero();
                    for (Atomo atomo: aminoacido.get_atomos()) {
                        cout << "\n\t\t Atomo -> " << atomo.get_nombre() << "-" << atomo.get_numero() << "\n\t\t\tCoordenadas -> [" << 
                        atomo.get_coordenada().get_x() << ", " << atomo.get_coordenada().get_y() << ", " <<
                        atomo.get_coordenada().get_z() << "] " << endl;
                    }
                }
            }
                            
        }
        
    }
}

//Funcion para mostrar las proteinas (lista) en pantalla
void lee_proteina(list <Proteina> proteinas) {
    for (Proteina prot: proteinas){
        cout << "\n-> " << prot.get_nombre();
    }
}

//Funcion con los procesos para 
void interaccion_proteinas() {
    
    //Declaracion de variables
    list <Proteina> proteinas;
    int var;
    string opcionSTR;
    int opcion;
    string nombre_proteina, id_proteina;
    string nombre_cadena, nombre_aminoacido;
    int numero_aminoacido;
    string nombre_atomo;
    int numero_atomo;
    float eje_x, eje_y, eje_z;
    string seleccion_proteina;


    //INICIO DEL MENÚ
    do{
        cout << "\n\n\t\t\t <<< Menu >>>" << endl;
        cout << "1. Leer datos proteinas \n" << endl;
        cout << "2. Imprimir proteina \n" << endl;
        cout << "3. Añadir proteina \n" << endl;
        cout << "4. Salir \n " << endl;
        cout << "Opcion: ";
        cin >> opcionSTR;
        opcion = atoi(opcionSTR.c_str());


        switch (opcion) {
            
            //Opcion para entrar a la impresion de las proteinas ingresadas (solo nomrbres)
            case 1: {
                cout << "\n\t\t-Lectura de datos proteicos-\n" << endl; 
                lee_proteina(proteinas);
            } break;

            //Opcion para imprimir una proteina determinada  
            case 2:{
                cout << "\n\t\t-Impresión de datos proteicos \n" << endl;
                cout << "Ingrese nombre de la proteína que desee visualizar:\n ";
                cout << "\n Proteinas disponibles: \n";
                lee_proteina(proteinas);

                cout << "\n\nEscriba aqui: ";
                cin >> seleccion_proteina;
                imprime_proteina(proteinas,seleccion_proteina);

            } break;

            //Opcion para generar una proteina
            case 3:{
                cout << "\t\t-Adicion de proteinas- \n" << endl;
                cout << "Ingrese nombre de la proteína: ";
                cin >> nombre_proteina;
                cout << "Ingrese ID de la proteína: ";
                cin >> id_proteina;
                Proteina proteina = Proteina(nombre_proteina, id_proteina);
                        
                int tecla1,tecla2,tecla3,tecla4,tecla5;

                //Ingreso de las CADENAS
                do {                 
                    cout << "Ingrese la letra de la cadena: ";
                    cin >> nombre_cadena;
                    Cadena cadena = Cadena(nombre_cadena);
                      
                    //Ingreso de los AMINOACIDOS
                    do {
                        cout << "Ingrese el nombre del aminoacido: ";
                        cin >> nombre_aminoacido;
                        cout << "Ingrese el numero del aminoacido: ";
                        cin >> numero_aminoacido; 
                        Aminoacido aminoacido = Aminoacido(nombre_aminoacido, numero_aminoacido);    

                        //Ingreso de los ATOMOS
                        do {
                            cout << "Ingrese el nombre del atomo: ";
                            cin >> nombre_atomo;
                            cout << "Ingrese el numero del atomo: ";
                            cin >> numero_atomo; 
                            Atomo atomo = Atomo(nombre_atomo, numero_atomo); 
                              
                            //Ingreso de las COORDENADAS
                            do{
                                cout << "Ingrese eje x de la coordenada: ";
                                cin >> eje_x; 
                                cout << "Ingrese eje y de la coordenada: ";
                                cin >> eje_y; 
                                cout << "Ingrese eje z de la coordenada: ";
                                cin >> eje_z; 
                                Coordenada coordenada = Coordenada(eje_x, eje_y, eje_z);    
                                
                                //Ingresa coordenada a lista y pregunta si desea ingresar otra
                                atomo.set_coordenada(coordenada);
                                cout << "Desea cargar otras coordenadas?";
                                cout << "\n 1. Si ";
                                cout << "\n 2. No ";
                                cout << "\nOpcion: ";
                                cin >> tecla4;

                            } while (tecla4 != 2);
                            //Se ingresa atomo en lista 
                            aminoacido.add_atomo(atomo);
                            //Opcion para volver a ingresar otro atomo
                            cout << "Desea cargar otro atomo?";
                            cout << "\n 1. Si";
                            cout << "\n 2. No";
                            cout << "\nOpcion: ";
                            cin >> tecla3;
                        
                        } while(tecla3 != 2);
                        //Se ingresa aminoacido en lista 
                        cadena.add_aminoacido(aminoacido);
                        //Opcion para volver a ingresar otro aminoacido
                        cout << "Desea cargar otro aminoacido?";
                        cout << "\n 1. Si";
                        cout << "\n 2. No";
                        cout << "\nOpcion: ";
                        cin >> tecla2;
                    
                    } while(tecla2 != 2);
                    
                    //Se ingresa cadena a lista cadena
                    proteina.add_cadena(cadena);
                    //Opcion para volver a ingresar otra cadena
                    cout << "Desea cargar otra cadena?";
                    cout << "\n 1. Si";
                    cout << "\n 2. No";
                    cout << "\nOpcion: ";
                    cin >> tecla1;
            
                } while(tecla1 != 2);
                //Ingreso de proteina a lista
                proteinas.push_back(proteina);
                break;
            }     
            
            //Opcion para salir del programa
            case 4: {           
                cout << "\nHa salido del programa \n \n";
            }
            
            default:
                break;  
        }
        
    }
    while(opcion != 4);
}

//Funcion que ejecutara el programa
int main() {
    interaccion_proteinas();
    return 0;
}