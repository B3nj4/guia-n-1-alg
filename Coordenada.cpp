#include <iostream>
using namespace std;

//Importe de archivos necesarios
#include "Coordenada.h"

//Variables
Coordenada::Coordenada() {
    float x = 0.0;
    float y = 0.0;
    float z = 0.0;
}

//Constructor
Coordenada::Coordenada(float x, float y, float z){
    this->x = x;
    this->y = y;
    this->z = z;
}

//Metodos
float Coordenada::get_x(){
    return this->x;
}
void Coordenada::set_x(float xNuevo) {
    x = xNuevo;
}

float Coordenada::get_y(){
    return this->y;
}
void Coordenada::set_y(float yNuevo) {
    y = yNuevo;
}

float Coordenada::get_z(){
    return this->z;
}
void Coordenada::set_z(float zNuevo) {
    z = zNuevo;
}

