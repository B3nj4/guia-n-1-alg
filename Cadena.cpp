#include <iostream>
using namespace std;

//Librerias necesarias
#include <list>

//Importe de archivos necesarios
#include "Cadena.h"
#include "Aminoacido.h"

//Variables
Cadena::Cadena() {
    string letra = "\0";
    list<Aminoacido> aminoacidos;
}

//Constructor
Cadena::Cadena(string letra){
    this->letra = letra;    
}

//Metodos
string Cadena::get_letra(){
    return this->letra;
}
list<Aminoacido> Cadena::get_aminoacidos(){
    return this->aminoacidos;
}

void Cadena::set_letra(string letraNuevo) {
    letra = letraNuevo;
}
void Cadena::add_aminoacido(Aminoacido aminoacidoNuevo) {
   aminoacidos.push_back(aminoacidoNuevo);
}

    