#ifndef CADENA_H
#define CADENA_H
using namespace std;

//Librerias necesarias
#include <string>
#include <list>
#include "Aminoacido.h"

class Cadena {
    private:
        //Variables
        string letra = "\0";
        list<Aminoacido> aminoacidos;

    public:
        //Constructor
        Cadena();
        Cadena(string letra);

        //Metodos
        string get_letra();
        list<Aminoacido> get_aminoacidos();
        void set_letra(string letraNuevo);
        void add_aminoacido(Aminoacido aminoacidoNuevo);

};
#endif
