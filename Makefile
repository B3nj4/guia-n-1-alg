prefix=/usr/local
CC = g++
CFLAGS = -g -Wall 
SRC = Main.cpp Proteina.cpp Cadena.cpp Aminoacido.cpp Atomo.cpp Coordenada.cpp
OBJ = Main.o Proteina.o Cadena.o Aminoacido.o Atomo.o Coordenada.o
APP = Main

all: $(OBJ)
	$(CC) $(CFLAGS)-o $(APP) $(OBJ) 
clean:
	$(RM) $(OBJ) $(APP)
install: $(APP)
	install -m 0755 $(APP) $(prefix)/bin
uninstall: $(APP)
	$(RM) $(prefix)/bin/$(APP)