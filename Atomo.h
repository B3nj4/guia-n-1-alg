#ifndef ATOMO_H
#define ATOMO_H
using namespace std;

//Librerias necesarias
#include <string>
#include <list>
#include "Coordenada.h"

class Atomo {
    private:
        //Variables
        string nombre;
        int numero;
        Coordenada coordenada;

    public:
        //Constructor
        Atomo();
        Atomo(string nombre, int numero);

        //Metodos
        string get_nombre();
        int get_numero();
        Coordenada get_coordenada();
        void set_nombre(string nombreNuevo) ;
        void set_numero(int numeroNuevo);
        void set_coordenada(Coordenada coordenadaNuevo);
   
};
#endif
